import * as React from 'react';
import { AnalyticsPresenter } from '../components/analytics.presenter';

export class HomeScreen extends React.Component<any> {
    render(): React.ReactNode {
        return <div>
            <AnalyticsPresenter clientId={this.props.clientId}/>
        </div>;
    }
}
