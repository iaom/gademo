import { get } from 'lodash';
import * as React from 'react';

const Gapi: any = (window as any).gapi;

export class AnalyticsPresenter extends React.Component<any> {
    private weeklyStatsMetrics = [
        'ga:sessions',
        'ga:bounces',
        'ga:sessionDuration',
        'ga:uniqueDimensionCombinations',
        'ga:hits',
        'ga:bounceRate',
        'ga:avgSessionDuration'
    ];

    public state: any = {
        activeUser: null,
        weeklyStats: null,
        isAuthorized: Gapi.analytics.auth && Gapi.analytics.auth.isAuthorized()
    };

    private async handleAnalyticsReady() {
        const viewSelector = new Gapi.analytics.ViewSelector({
            container: 'view-selector'
        });

        viewSelector.on('change', (ids) => this.handleViewSelectorChanges(ids));

        // Auth
        Gapi.analytics.auth.on('success', () => {
            this.updateIsAuthorized();
            viewSelector.execute();
        });

        Gapi.analytics.auth.authorize({
            container: 'auth-button',
            clientid: this.props.clientId
        });
    }

    private handleViewSelectorChanges(ids) {
        this.setActiveUsers(ids);
        this.setWeeklyStats(ids);
    }

    private updateIsAuthorized(force?: boolean | undefined) {
        this.setState({
            isAuthorized: (force === undefined) ? Gapi.analytics.auth.isAuthorized() : force
        });
    }

    private async signOut() {
        try {
            const auth2 = Gapi.auth2.getAuthInstance();
            await auth2.signOut();
            location.reload(true);
        } catch (err) {
            alert('Something went wrong (GASignOut)');
        }
    }

    private async setActiveUsers(ids) {
        try {
            const response = await Gapi.client.analytics.data.realtime.get({
                ids: ids,
                metrics: "rt:activeUsers"
            });

            this.setState({
                activeUser: response && response.result
            });
        } catch (err) {
            alert('Something went wrong (GAActiveUsers)');
        }
    }

    private async setWeeklyStats(ids) {
        try {
            const report = new Gapi.analytics.report.Data({
                query: {
                    ids: ids,
                    metrics: this.weeklyStatsMetrics.toString(),
                    dimensions: 'ga:date'
                }
            });

            report.on('error', (err) => {
                throw err;
            });

            report.on('success', (response: any) => {
                this.setState({
                    weeklyStats: response
                });
            });

            report.execute();
        } catch (err) {
            alert('Something went wrong (GAALastWeekSession)');
        }
    }

    componentDidMount(): void {
        if (!Gapi || !Gapi.analytics) {
            alert('Network Error (no google api found)');
        }

        Gapi.analytics.ready(() => this.handleAnalyticsReady());
    }

    render(): React.ReactNode {
        return <div className="container">

            <div className="row">
                <div className="col-12 pt-5 pb-5 d-flex justify-content-center align-items-center">
                    {this.state.isAuthorized &&
                    <button className="btn btn-danger" onClick={() => this.signOut()}>SignOut</button>}
                    {!this.state.isAuthorized && <section id="auth-button"/>}
                </div>

                {this.state.isAuthorized && <div className="col-12 pt-5">
                    <section id="view-selector"/>
                </div>}

                {this.state.isAuthorized && <div className="col-12 pt-5">
                    <div className="alert alert-warning">Active
                        Users: <b>{get(this.state.activeUser, 'totalsForAllResults[rt:activeUsers]')}</b></div>
                </div>}

                {this.state.isAuthorized && <div className="col-12 pt-5 overflow-auto">
                    <table className="table-condensed table">
                        <thead>
                        <tr>
                            {this.state.weeklyStats && this.state.weeklyStats.columnHeaders.map((row: any, index: number) =>
                                <th key={index}>{row.name}</th>)}
                        </tr>
                        </thead>

                        <tbody>
                        {this.state.weeklyStats && this.state.weeklyStats.rows.map((row: any, index: number) =>
                            <tr key={index}>
                                {row.map((cell: string, cellIndex: number) => <td key={cellIndex}>{cell}</td>)}
                            </tr>)}
                        </tbody>
                    </table>
                </div>}

            </div>

        </div>;
    }
}
