import * as React from 'react';
import * as ReactDOM from 'react-dom';
import '../node_modules/bootstrap/scss/bootstrap';
import { HomeScreen } from './screens/home.screen';

const locUrl = new URL(location.href);
const clientId: string = locUrl.searchParams.get('clientId') || '49649881242-45j62dbj7va7s3bf74f2pt6m7t7mkddc.apps.googleusercontent.com';

ReactDOM.render(<HomeScreen clientId={clientId}/>, document.getElementById('application'));
